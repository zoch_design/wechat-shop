
var customerhttp = getcustomerHttp();//定义接口常量

//获取地址中的参数
function getUrlParam(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r!=null) return unescape(r[2]); return null; //返回参数值
}
var cft_token;
var openid;
var code;
var code2;
var state;
code = getUrlParam('code');
code2 = localStorage.getItem('code');
state = getUrlParam('state');
console.log('code值=' + code);
cft_token = localStorage.getItem('token');
if(cft_token){//判断本地是否存储有token
    
    // switch (state){//根据菜单按钮传过来的值判断跳转至对应页面
    //     case 1:location.href = customerhttp + 'CFT-CUSTOMER/index.html';//下单页面
    //         break;
    //     case 2:location.href = customerhttp + 'CFT-CUSTOMER/shopInformation.html';//店铺首页
    //         break;
    //     case 3:location.href = customerhttp + 'CFT-CUSTOMER/myMenu.html';//我的页面
    //         break;
    // }
}else{//如果没有，则获取code值
    if(code == null || code == '' || code == undefined  || code == code2){//判断是否有code值，如果没有，则获取code的值
        location.href = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' + 'wxb53744eaa27c2549' + '&redirect_uri=' + 'http%3a%2f%2fcft.ihengtian.top%2fCFT-CUSTOMER%2fLogin.html' + '&response_type='+ 'code' + '&scope=' + 'snsapi_base' + '&state=' + state + '#wechat_redirect';
    }else{//如果有code，则从后台获取openid
        $('.login-box').show();//显示登录页面
	localStorage.setItem('code',code);
        $.ajax({//将code传给后台同事获得openid的返回值
            type:'GET',
            url:customerhttp + 'api/cft/wechat/get/openid',
            data:{'code':code},
            dataType:"json",
            success:function (res) {
                openid = res.data;
                console.log('openid=' + openid);
            },
            error:function (res) {
                layer.msg('登陆错误，请重新打开页面');
            }
        });
    }
}

//用户输入登录信息



$("#nickx-icon").css('display','none');
//监听文本框变化事件
$("#userphone").bind('input',function () {
    var l = $("#userphone").val().length;
    if(l && l>0){
        $("#nickx-icon").css('display','block');
    }else{
        $("#nickx-icon").css('display','none');
    }
});

//清除输入框
$("#nickx-icon").click(function () {
    $("#userphone").val("");
});
// 点击登录
$('#login').click(function () {
    //定义用户名和验证码
    var userphone = $('#userphone').val();
    var security = $('#security').val();
    if(userphone == ''){
        layer.msg('请输入手机号');
    }else if(security ==''){
        layer.msg('请输入验证码');
    }else{
        $.ajax({
           type:'post',
           url:customerhttp + 'api/cft/customer/login',
           contentType:'application/json',
           data:JSON.stringify({'mobile': userphone , 'verify': security ,'openId':openid}),
           dataType:'json',
           success:function (res) {
               cft_token = res.data;//获取返回值token
               console.log(cft_token);
               localStorage.setItem('token',cft_token);//存储token
               console.log(localStorage.getItem('token'));
               // switch (state){//根据菜单按钮传过来的值判断跳转至对应页面
               //     case 1:location.href = customerhttp + 'CFT-CUSTOMER/index.html';
               //         break;
               //     case 2:location.href = customerhttp + 'CFT-CUSTOMER/shopInformation.html';
               //         break;
               //     case 3:location.href = customerhttp + 'CFT-CUSTOMER/myMenu.html';
               //         break;
               // }
           },
           error:function (res) {
                layer.msg('登陆错误，请重新打开页面');
           }
        });

    }

});